package com.ont.receivermicroservice.controller;

import com.ont.receivermicroservice.model.receiver;
import com.ont.receivermicroservice.service.receiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/receiver/*")
public class receiverController {
    @Autowired
    private receiverService receiverService;

    @GetMapping(value = "get")
    String get(){
        return "Test";
    }

    @GetMapping(value = "getAll")
    List<receiver> getAll(){
        List<receiver> receivers=receiverService.getAll();
        return receivers;
    }
}
