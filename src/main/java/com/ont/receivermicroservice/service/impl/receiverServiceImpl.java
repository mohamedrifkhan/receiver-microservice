package com.ont.receivermicroservice.service.impl;

import com.ont.receivermicroservice.model.receiver;
import com.ont.receivermicroservice.repo.receiverRepo;
import com.ont.receivermicroservice.service.receiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class receiverServiceImpl implements receiverService {
    @Autowired
    private receiverRepo receiverRepo;

    @Override
    public List<receiver> getAll() {
        List<receiver> receivers=receiverRepo.findAll();
        return receivers;
    }
}
