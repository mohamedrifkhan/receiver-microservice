package com.ont.receivermicroservice.repo;

import com.ont.receivermicroservice.model.receiver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface receiverRepo extends JpaRepository<receiver,Long> {
}
