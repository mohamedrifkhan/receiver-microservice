package com.ont.receivermicroservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "receivers")
public class receiver {
    @Id
    @Column(name = "id")
    private long id;
    @Column(name = "cip")
    private String cip;
    @Column(name = "nssi")
    private String nssi;
    @Column(name = "dni")
    private String dni;
    @Column(name = "fecha_trasplante")
    private Date transplantDate;
    @Column(name = "codigo_hospital_trasplantador")
    private long transplantHospitalCode;
    @Column(name = "hospital_trasplantador")
    private String transplantHospital;
    @Column(name = "tipo_trasplante")
    private String transplantType;
    @Column(name = "fecha_nacimiento")
    private Date dateOfBirth;
    @Column(name = "sexo")
    private String sex;
    @Column(name = "grupo_sanguineo")
    private String bloodType;
    @Column(name = "pais_nacimiento")
    private String countryOfBirth;
    @Column(name = "donante_id")
    private long donorId;
    @Column(name = "drne")
    private int drne;
    @Column(name = "vigilancia")
    private int surveillance;
    @Column(name = "procesado_drne")
    private int indictedDrne;
    @Column(name = "procesado_vigilancia")
    private int indictedSurveillance;

    public receiver() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCip() {
        return cip;
    }

    public void setCip(String cip) {
        this.cip = cip;
    }

    public String getNssi() {
        return nssi;
    }

    public void setNssi(String nssi) {
        this.nssi = nssi;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public Date getTransplantDate() {
        return transplantDate;
    }

    public void setTransplantDate(Date transplantDate) {
        this.transplantDate = transplantDate;
    }

    public long getTransplantHospitalCode() {
        return transplantHospitalCode;
    }

    public void setTransplantHospitalCode(long transplantHospitalCode) {
        this.transplantHospitalCode = transplantHospitalCode;
    }

    public String getTransplantHospital() {
        return transplantHospital;
    }

    public void setTransplantHospital(String transplantHospital) {
        this.transplantHospital = transplantHospital;
    }

    public String getTransplantType() {
        return transplantType;
    }

    public void setTransplantType(String transplantType) {
        this.transplantType = transplantType;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public String getCountryOfBirth() {
        return countryOfBirth;
    }

    public void setCountryOfBirth(String countryOfBirth) {
        this.countryOfBirth = countryOfBirth;
    }

    public long getDonorId() {
        return donorId;
    }

    public void setDonorId(long donorId) {
        this.donorId = donorId;
    }

    public int getDrne() {
        return drne;
    }

    public void setDrne(int drne) {
        this.drne = drne;
    }

    public int getSurveillance() {
        return surveillance;
    }

    public void setSurveillance(int surveillance) {
        this.surveillance = surveillance;
    }

    public int getIndictedDrne() {
        return indictedDrne;
    }

    public void setIndictedDrne(int indictedDrne) {
        this.indictedDrne = indictedDrne;
    }

    public int getIndictedSurveillance() {
        return indictedSurveillance;
    }

    public void setIndictedSurveillance(int indictedSurveillance) {
        this.indictedSurveillance = indictedSurveillance;
    }

    @Override
    public String toString() {
        return "receiver{" +
                "id=" + id +
                ", cip='" + cip + '\'' +
                ", nssi='" + nssi + '\'' +
                ", dni='" + dni + '\'' +
                ", transplantDate=" + transplantDate +
                ", transplantHospitalCode=" + transplantHospitalCode +
                ", transplantHospital='" + transplantHospital + '\'' +
                ", transplantType='" + transplantType + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", sex='" + sex + '\'' +
                ", bloodType='" + bloodType + '\'' +
                ", countryOfBirth='" + countryOfBirth + '\'' +
                ", donorId=" + donorId +
                ", drne=" + drne +
                ", surveillance=" + surveillance +
                ", indictedDrne=" + indictedDrne +
                ", indictedSurveillance=" + indictedSurveillance +
                '}';
    }
}
