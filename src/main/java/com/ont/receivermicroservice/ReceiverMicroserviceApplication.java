package com.ont.receivermicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ReceiverMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReceiverMicroserviceApplication.class, args);
	}

}
